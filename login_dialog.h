#ifndef LOGIN_DIALOG_H
#define LOGIN_DIALOG_H

#include <QDialog>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include "mainwindow.h"

namespace Ui {
class Login_Dialog;
}

class Login_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Login_Dialog(QSqlDatabase db, QWidget *parent = nullptr);
    ~Login_Dialog();

private slots:
    void on_tabWidget_currentChanged(int index);

    void on_bttn_exit_clicked();

    void on_bttn_confirm_clicked();

private:
    Ui::Login_Dialog *ui;
    bool mode_;
    QSqlDatabase* db_;
    MainWindow* parent_;
};

#endif // LOGIN_DIALOG_H
