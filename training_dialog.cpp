#include "training_dialog.h"
#include "ui_training_dialog.h"
#include <iostream>
using namespace std;

training_dialog::training_dialog(QSqlDatabase db, int uid, QString item, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::training_dialog)
{
    ui->setupUi(this);

    db_ = &db;
    uid_ = uid;
    counter_ = 1;

    query_ = new QSqlQuery(*db_);

    table_ = "user" + QString::number(uid_) + "_" + item;
    query_->exec("SELECT front, back FROM " + table_);
    query_->next();

    ui->front->setText(query_->value(0).toString());
    ui->progressBar->setValue(0);
}

training_dialog::~training_dialog()
{
    delete ui;
}

void training_dialog::on_bttn_next_clicked()
{

    QString back = query_->value(1).toString();
    if (ui->back->toPlainText() != back)
    {
        QMessageBox::critical(this,
            tr("MemorizeIt"),
            tr("Uncorrect! Correct is: " + back.toUtf8()),
            tr("next"));
        QSqlQuery query2(*db_);
        query2.exec("UPDATE " + table_ + " SET lastTraining=NOW(), trainings=trainings+1"
                                         " WHERE front='" + ui->front->toPlainText() + "'");

    }
    else {
        QSqlQuery query2(*db_);
        query2.exec("UPDATE " + table_ + " SET lastTraining=NOW(), trainings=trainings+1, successTrainings=successTrainings+1"
                                         " WHERE front='" + ui->front->toPlainText() + "'");
    }

    ui->progressBar->setValue((int)(counter_++ / query_->size() * 100));
    ui->back->clear();

    if (query_->next())
        ui->front->setText(query_->value(0).toString());
    else
        accept();
}

void training_dialog::on_bttn_cancel_clicked()
{
    reject();
}
