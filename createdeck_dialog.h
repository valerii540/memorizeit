#ifndef CREATEDECK_DIALOG_H
#define CREATEDECK_DIALOG_H

#include <QDialog>
#include <QLinkedList>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QMessageBox>

struct Card
{
    QString front;
    QString back;
};

namespace Ui {
class CreateDeck_dialog;
}

class CreateDeck_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDeck_dialog(QSqlDatabase db, int uid, QWidget *parent = nullptr);
    ~CreateDeck_dialog();

private slots:
    void on_bttn_add_clicked();
    //QLinkedList<Card> getDeck();

    void on_bttn_ok_clicked();

    void on_bttn_cancel_clicked();

private:
    Ui::CreateDeck_dialog *ui;

    QSqlDatabase* db_;
    int count_;
    int uid_;
    QLinkedList<Card> list_;
};

#endif // CREATEDECK_DIALOG_H
